import 'package:flutter/foundation.dart';

class PlaceLocation {
  final double latitude;
  final double longitude;

  PlaceLocation({@required this.latitude, @required this.longitude});

  String printLocation() {
    return latitude.toString() + '||' + longitude.toString();
  }
}

class Place {
  final String title;
  final PlaceLocation location;

  Place({@required this.title, @required this.location});
}

class MyPlaces {
  List<Place> _places = [
    Place(
        title: 'Moscow - Red Square',
        location: PlaceLocation(latitude: 55.7539268, longitude: 37.6208426)),
    Place(
        title: 'Berlin - Brandenburg Gate',
        location: PlaceLocation(latitude: 52.5165386, longitude: 13.3777376)),
    Place(
        title: 'Tokyo - Imperial Palace',
        location: PlaceLocation(latitude: 35.6823477, longitude: 139.7533474)),
  ];

  List<Place> get places {
    return [..._places];
  }
}
