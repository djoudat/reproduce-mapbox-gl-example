import 'package:flutter/material.dart';

import '../views/display_map_screen.dart';
import '../models/place.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Container(
        child: Center(
          child: Column(
            children: <Widget>[
              RaisedButton(
                child: Text(MyPlaces().places[0].title),
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (ctx) => DisplayMapScreen(MyPlaces().places[0]),
                    ),
                  );
                },
              ),
              RaisedButton(
                child: Text(MyPlaces().places[1].title),
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (ctx) => DisplayMapScreen(MyPlaces().places[1]),
                    ),
                  );
                },
              ),
              RaisedButton(
                child: Text(MyPlaces().places[2].title),
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (ctx) => DisplayMapScreen(MyPlaces().places[2]),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
