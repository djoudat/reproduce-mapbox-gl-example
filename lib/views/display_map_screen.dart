import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:mapbox_gl/mapbox_gl.dart';

import '../models/place.dart';

class DisplayMapScreen extends StatelessWidget {
  MapboxMapController controller;
  Place place;

  DisplayMapScreen(this.place);

  Future<void> _onMapCreated(MapboxMapController controller) async {
    this.controller = controller;

    CameraUpdate newPosition = CameraUpdate.newCameraPosition(
      CameraPosition(
          target: LatLng(place.location.latitude, place.location.longitude),
          tilt: 40,
          zoom: 16),
    );

    try {
      await controller.animateCamera(newPosition);
    } catch (err) {
      print('err message: ${err.message}');
    }
  }

  void _onStyleLoadedCallback() async {
    SymbolOptions options = SymbolOptions(
      geometry: LatLng(place.location.latitude, place.location.longitude),
      iconImage: 'airport-15',
    );
    try {
      await controller.addSymbol(options);
    } on PlatformException catch (err) {
      print('PlatformException: ${err.message}');
    } catch (err) {
      print('err message: ${err.message}');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(place.title),
      ),
      body: Container(
        child: Center(
          child: MapboxMap(
            initialCameraPosition: const CameraPosition(
                target: LatLng(55.0, 55.0), tilt: 40, zoom: 16),
            onMapCreated: _onMapCreated,
            onStyleLoadedCallback: _onStyleLoadedCallback,
          ),
        ),
      ),
    );
  }
}
